package com.app.controller;

import com.app.model.AddressDTO;
import com.app.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/address")
public class AddressController {

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @PostMapping
    public ResponseEntity<AddressDTO> create(@RequestBody final AddressDTO addressDTO) {
        return
                ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(addressService.create(addressDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AddressDTO> getById(@PathVariable Long id) {

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(addressService.getById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<AddressDTO> update(@PathVariable Long id,
                                             @RequestBody AddressDTO addressDTO) {
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(addressService.update(id, addressDTO));
    }


    @PatchMapping("/{id}")
    public ResponseEntity<AddressDTO> partialUpdate(@PathVariable Long id,
                                                    @RequestBody AddressDTO addressDTO) {
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(addressService.partialUpdateStreet(id, addressDTO.getStreet()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Long id) {

        addressService.delete(id);

        return ResponseEntity
                .status(HttpStatus.OK)
                .build();
    }
}
