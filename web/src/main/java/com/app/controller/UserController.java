package com.app.controller;


import com.app.model.UserDTO;
import com.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<UserDTO> create(@RequestBody final UserDTO userDTO) {
        return
                ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body(userService.create(userDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> getById(@PathVariable Long id) {

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userService.getById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> update(@PathVariable Long id,
                                          @RequestBody UserDTO userDTO) {
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(userService.update(id, userDTO));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<UserDTO> partialUpdate(@PathVariable Long id,
                                                 @RequestBody UserDTO userDTO) {
        return ResponseEntity
                .status(HttpStatus.ACCEPTED)
                .body(userService.partialUpdateName(id, userDTO.getName()));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Long id) {

        userService.delete(id);

        return ResponseEntity
                .status(HttpStatus.OK)
                .build();
    }

}
