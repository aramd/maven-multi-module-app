package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MavenMultiModuleApp {
    public static void main(String[] args) {
        SpringApplication.run(MavenMultiModuleApp.class, args);
    }
}
