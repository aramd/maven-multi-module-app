package com.app.service.impl;

import com.app.entity.Address;
import com.app.model.AddressDTO;
import com.app.repository.AddressRepository;
import com.app.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }


    @Override
    public AddressDTO create(AddressDTO addressDTO) {

        Address address = mapToAddress(addressDTO);
        address.setDateCreated(Instant.now());
        addressRepository.save(address);

        return mapToAddressDto(address);
    }

    @Override
    public AddressDTO getById(Long id) {
        Address address = addressRepository.findById(id).orElseThrow();

        return mapToAddressDto(address);
    }

    @Override
    public AddressDTO update(Long id, AddressDTO addressDTO) {

        Address address = mapToAddress(id, addressDTO);
        address.setDateUpdated(Instant.now());
        addressRepository.save(address);

        return mapToAddressDto(id, address);

    }

    @Override
    public AddressDTO partialUpdateStreet(Long id, String street) {
        Address address = addressRepository.findById(id).orElseThrow();
        address.setStreet(street);
        addressRepository.save(address);

        return mapToAddressDto(address);
    }

    @Override
    public void delete(Long id) {

        addressRepository.deleteById(id);

    }

    private Address mapToAddress(AddressDTO addressDTO) {

        Address address = new Address();
        address.setId(addressDTO.getId());
        address.setCity(addressDTO.getCity());
        address.setStreet(addressDTO.getStreet());

        return address;
    }


    private Address mapToAddress(Long id, AddressDTO addressDTO) {

        Address address = new Address();
        address.setId(addressDTO.getId());
        address.setCity(addressDTO.getCity());
        address.setStreet(addressDTO.getStreet());

        return address;
    }

    protected AddressDTO mapToAddressDto(Address address) {
        final AddressDTO addressDTO = new AddressDTO();

        addressDTO.setId(address.getId());
        addressDTO.setCity(address.getCity());
        addressDTO.setStreet(address.getStreet());

        return addressDTO;
    }

    private AddressDTO mapToAddressDto(Long id, Address address) {
        final AddressDTO addressDTO = new AddressDTO();

        addressDTO.setId(address.getId());
        addressDTO.setCity(address.getCity());
        addressDTO.setStreet(address.getStreet());

        return addressDTO;
    }

}
