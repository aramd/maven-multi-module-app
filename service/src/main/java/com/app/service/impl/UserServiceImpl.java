package com.app.service.impl;

import com.app.entity.User;
import com.app.mapper.AddressMapper;
import com.app.model.AddressDTO;
import com.app.model.UserDTO;
import com.app.repository.UserRepository;
import com.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AddressServiceImpl addressServiceImpl;

    private final AddressMapper addressMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, AddressMapper addressMapper, AddressServiceImpl addressServiceImpl, AddressMapper addressMapper1) {
        this.userRepository = userRepository;
        this.addressServiceImpl = addressServiceImpl;
        this.addressMapper = addressMapper;

    }


    @Override
    public UserDTO create(UserDTO userDTO) {
        User user = mapToUser(userDTO);
        user.setDateCreated(Instant.now());
        userRepository.save(user);

        return mapToUserDto(user);
    }

    @Override
    public UserDTO getById(Long id) {
        User user = userRepository.findById(id).orElseThrow();

        return mapToUserDto(user);
    }

    @Override
    public UserDTO update(Long id, UserDTO userDTO) {
        User user = mapToUser(id, userDTO);
        user.setDateUpdated(Instant.now());
        userRepository.save(user);

        return mapToUserDto(id, user);
    }

    @Override
    public UserDTO partialUpdateName(Long id, String name) {
        User user = userRepository.findById(id).orElseThrow();
        user.setName(name);
        userRepository.save(user);

        return mapToUserDto(user);
    }

    @Override
    public void delete(Long id) {

        userRepository.deleteById(id);

    }

    private User mapToUser(UserDTO userDTO) {

        User user = new User();
        user.setId(userDTO.getId());
        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());

        return user;
    }


    private User mapToUser(Long id, UserDTO userDTO) {

        User user = new User();
        user.setId(userDTO.getId());
        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());

        return user;
    }

    private UserDTO mapToUserDto(User user) {
        final UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setSurname(user.getSurname());

        AddressDTO addressDTO = addressServiceImpl.mapToAddressDto(user.getAddress());
        userDTO.setAddressDTO(addressDTO);

        return userDTO;
    }

    private UserDTO mapToUserDto(Long id, User user) {
        final UserDTO userDTO = new UserDTO();

        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setSurname(user.getSurname());

        return userDTO;
    }
}
