package com.app.service;

import com.app.model.AddressDTO;


public interface AddressService {

    AddressDTO create(AddressDTO address);

    AddressDTO getById(Long id);

    AddressDTO update(Long id, AddressDTO address);

    AddressDTO partialUpdateStreet(Long id, String street);

    void delete(Long id);
}
