package com.app.service;

import com.app.model.UserDTO;

public interface UserService {

    UserDTO create(UserDTO user);

    UserDTO getById(Long id);

    UserDTO update(Long id, UserDTO user);

    UserDTO partialUpdateName(Long id, String name);

    void delete(Long id);
}
