package com.app.model;

public class UserDTO {

    private Long id;
    private String name;
    private String surname;
    private AddressDTO addressDTO;

    public UserDTO() {
    }

    public UserDTO(Long id, String name, String surname, AddressDTO addressDTO) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.addressDTO = addressDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public AddressDTO getAddressDTO() {
        return addressDTO;
    }

    public void setAddressDTO(AddressDTO addressDTO) {
        this.addressDTO = addressDTO;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", addressDTO=" + addressDTO +
                '}';
    }
}
