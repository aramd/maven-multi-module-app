package com.app.mapper;

import com.app.entity.Address;
import com.app.model.AddressDTO;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    public AddressDTO mapToAddressDto(Address address) {
        final AddressDTO addressDTO = new AddressDTO();

        addressDTO.setId(address.getId());
        addressDTO.setCity(address.getCity());
        addressDTO.setStreet(address.getStreet());

        return addressDTO;
    }
}
