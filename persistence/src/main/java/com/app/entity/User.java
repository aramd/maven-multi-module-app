package com.app.entity;


import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "tbl_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "data_created")
    private Instant dateCreated;

    @Column(name = "data_updated")
    private Instant dateUpdated;

    @Column(name = "data_removed")
    private Instant dateRemoved;

    @OneToOne
    @JoinColumn(name = "address_id")
    private Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }


    public User(Long id, String name, String surname, Instant dateCreated, Instant dateUpdated, Instant dateRemoved) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.dateRemoved = dateRemoved;
    }

    public User() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Instant getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Instant dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Instant getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Instant dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Instant getDateRemoved() {
        return dateRemoved;
    }

    public void setDateRemoved(Instant dateRemoved) {
        this.dateRemoved = dateRemoved;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateUpdated=" + dateUpdated +
                ", dateRemoved=" + dateRemoved +
                '}';
    }
}
